# DDG-noJS
Website containing OpenSearch descriptions for adding non-JavaScript
DuckDuckGoOnion search engines to the Tor Browser.

https://ddg-nojs.codeberg.page


## License
[CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)


## Notes
* `favicon.ico` is based on https://www.w3.org/icons/index.png (license: public domain).
    Converted from PNG to ICO using GIMP.
* `opensearch-ddg-onion-html.xml` is adapted from https://duckduckgo.com/opensearch_html_v2.xml
    (retrieved on 2022-05-01).
* `opensearch-ddg-onion-lite.xml` is adapted from https://duckduckgo.com/opensearch_lite_v2.xml
    (retrieved on 2022-05-01).
